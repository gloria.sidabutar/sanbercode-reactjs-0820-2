import React from 'react';
import './App.css';
import Tugas9 from './Tugas-9/tugas9';
import Tugas10 from './Tugas-10/tugas10';
import Tugas11 from './Tugas-11/tugas11';
//import DaftarBuah from './Tugas-13/tugas13';
import DaftarBuah from './Tugas-14/DaftarBuah';

import axios from 'axios';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Routes from "./Tugas-15/Routes";


function App() {
  return (
 
    <Router>
      <Routes/>
    </Router>   
    );
  }
  
export default App;
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Tugas9 from '../Tugas-9/tugas9';
import Tugas10 from '../Tugas-10/tugas10';
import Timer from '../Tugas-11/tugas11';
import DaftarBuah from '../Tugas-13/tugas13';
import DaftarBuah1 from '../Tugas-12/tugas12';
import "./nav.css"

export default function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/tugas9">Tugas9</Link>
            </li>
            <li>
              <Link to="/tugas11">Tugas10</Link>
            </li>
            <li>
              <Link to="/tugas10">Tugas11</Link>
            </li>
            <li>
              <Link to="/tugas12">Tugas12</Link>
            </li>
            <li>
              <Link to="/tugas13">Tugas13</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          
          <Route path="/tugas9" component={Tugas9}>
            <Tugas9 />
          </Route>
          <Route path="/tugas10" component={Tugas10}>
            <Tugas10 />
          </Route>
          <Route path="/tugas11" component={Timer}>
            <Timer />
          </Route>
          <Route path="/tugas12" component={DaftarBuah1}>
            <DaftarBuah1/>
          </Route>
          <Route path="/tugas13" component={DaftarBuah}>
            <DaftarBuah />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}


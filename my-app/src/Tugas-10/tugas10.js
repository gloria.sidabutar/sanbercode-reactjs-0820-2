import React from 'react';
import '../App.css';




class Tugas10 extends React.Component {
  render() {
    let dataHargaBuah = [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ]
    return ( 
    
    <div className="FormBuah" >
    <h1 style={{textAlign:"center"}}>Tabel Harga Buah</h1>
    
    <table className="tabel10">
      <tr style={{textAlign:"center"}}>
        <th><b>Nama</b></th>
        <th><b>Harga</b></th>
        <th><b>Berat</b></th>
      </tr>
      
      {dataHargaBuah.map(el=> {
          return (
            <>
            <tr>
            <td>{el.nama}</td>
            <td>{el.harga}</td>
            <td>{el.berat/1000} kg </td>
            </tr>
            </>
            
          )
        })}
      </table>
     </div>
    
    )
  }
}

export default Tugas10
